const validator = require('../helpers/validate');

const createEmployer = (req, res, next) => {
    const validationRule = {
        "firstName": "required|string",
        "lastName": "required|string",
        "phone": "required|string",
        "email": "required|email",
        "post": "required|string",
        "join_date":"required|date"
    }
    validator(req.body, validationRule, {}, (err, status) => {
        if (!status) {
            res.status(412)
                .send({
                    success: false,
                    message: 'Validation failed',
                    errorStack: err
                });
        } else {
            next();
        }
    });
}

module.exports = {
    createEmployer
}

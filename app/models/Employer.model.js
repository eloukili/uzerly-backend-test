module.exports = (sequelize, Sequelize) => {
    const moment = require('moment');

    const Employer = sequelize.define("employer", {
        firstName: {
            type: Sequelize.STRING
        },
        lastName: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING,
            unique:true
        },
        phone: {
            type: Sequelize.STRING
        },
        post:{
            type: Sequelize.STRING
        },
        join_date: {
            type: Sequelize.DATEONLY,
            get: function() {
                return moment.utc(this.getDataValue('join_date')).format('YYYY-MM-DD');
            }
        }
    });

    return Employer;
};

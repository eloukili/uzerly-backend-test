const db = require("../models");
const Employer = db.Employer;

// Calclate the pagination
const getPagination = (page, size) => {
    const limit = size ? parseInt(size) : 3;
    const offset = page ? page * limit : 0;

    return { limit, offset };
};
// Create and Save a new Employer
exports.create = (req, res) => {
    // check if user exist in database
    Employer.findOne({
        where:{
            email: req.body.email
        }
    }).then(empl =>{
        if(!empl){
            const employer = {
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                phone: req.body.phone,
                post: req.body.post,
                join_date: req.body.join_date
            };
            // Save Tutorial in the database
            Employer.create(employer)
                .then(data => {
                    res.status(201).send({success: true, message: "Employer created with success." ,data: data});
                })
                .catch(err => {
                    res.status(500).send({
                        success: false,
                        message: err.message || "Some error occurred while creating the Employer."
                    });
                });
        }
        else{
            res.status(400).send({
                success: false,
                message: "Email already exist"
            });
        }

    }).catch(error =>{
        res.status(400).send({
            success: false,
            message: error.message
        });
    })
};

// Retrieve all Employer from the database.
exports.findAll = (req, res) => {
    const {page, size} = req.query;
    let pageNumber = page ? parseInt(page) : 0;
    const { limit, offset } = getPagination(pageNumber, size);

    Employer.findAndCountAll({limit, offset})
        .then(employers =>{
            res.status(200).send({
                success: true,
                totalItems:employers.count,
                page: pageNumber,
                size: limit,
                employers: employers.rows
            })
        })
        .catch(error => {
            res.status(400).send({
                success: false,
                message: error.message
            })
        })
};

// Find a single Employer with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
    Employer.findByPk(id)
        .then(employer =>{
            res.status(200).send({
                success: true,
                employer: employer
            })
        })
        .catch(error =>{
            res.status(400).send({
                success: false,
                message: error.message
            })
        })
};

// Update a Employer by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    Employer.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.status(200).send({
                    success: true,
                    message: "Employer was updated successfully."
                });
            } else {
                res.status(400).send({
                    success: false,
                    message: `Cannot update Employer with id=${id}. Maybe Employer was not found or req.body is empty!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                success: false,
                message: "Error updating Employer with id=" + id
            });
        });
};

// Delete a Employer with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Employer.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num == 1) {
                res.status(200).send({
                    success: true,
                    message: "Employer was deleted successfully!"
                });
            } else {
                res.status(400).send({
                    success: false,
                    message: `Cannot delete Employer with id=${id}. Maybe Employer was not found!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                success: false,
                message: "Could not delete Employer with id=" + id
            });
        });
};

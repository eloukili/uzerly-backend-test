const validationMiddleware = require('../../middleware/validation-middleware');

module.exports = app => {
    const employers = require("../controllers/employer.controller.js");

    const router = require("express").Router();

    router.post("/", validationMiddleware.createEmployer, employers.create);
    router.get("/", employers.findAll);
    router.get("/:id", employers.findOne);
    router.put("/:id", validationMiddleware.createEmployer, employers.update);
    router.delete("/:id", employers.delete);

    app.use('/api/employers', router);
};
